﻿using Lab04_28092023.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ProgressBar;

namespace Lab04_28092023
{
    public partial class Form1 : Form
    {
        StudentContextDB db = new StudentContextDB();
        public Form1()
        {
            InitializeComponent();
            db = new StudentContextDB();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bạn có muốn thoát ?", "Thông báo !", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
            try
            {
                List<Faculty> listFac = db.Faculties.ToList();
                List<Student> listStu = db.Students.ToList();
                FillFalcultyCombobox(listFac);
                BindGrid(listStu);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void FillFalcultyCombobox(List<Faculty> listFalcultys)
        {
            this.cmbNganh.DataSource = listFalcultys;
            this.cmbNganh.DisplayMember = "FacultyName";
            this.cmbNganh.ValueMember = "FacultyID";
        }
        private void BindGrid(List<Student> listStudent)
        {
            dtgvStudent.Rows.Clear();
            int stt = 1;
            foreach (var item in listStudent)
            {
                int index = dtgvStudent.Rows.Add();
                dtgvStudent.Rows[index].Cells[0].Value = stt.ToString();
                dtgvStudent.Rows[index].Cells[1].Value = item.StudentID;
                dtgvStudent.Rows[index].Cells[2].Value = item.FullName;
                dtgvStudent.Rows[index].Cells[3].Value = item.AverageScore;
                dtgvStudent.Rows[index].Cells[4].Value = item.Faculty.FacultyName;
                stt++;
            }
        }
        private void txbID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        private void txbDTB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private bool IsStudentID(string studentID)
        {
            return studentID.Length == 10;
        }
        private void btnAddEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txbID.Text == "" || txbFullName.Text == "" || txbDTB.Text == "")
                {
                    if (txbDTB.Text == "")
                    {
                        MessageBox.Show("Mã sinh viên gồm 10 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    if (txbFullName.Text == "")
                    {
                        MessageBox.Show("Sai ho ten", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    if (txbDTB.Text == "")
                    {
                        MessageBox.Show("Sai diem trung binh", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    throw new Exception("Vui lòng nhập đầy đủ thông tin sinh viên!");
                }
                else if (IsStudentID(txbID.Text) == false || txbFullName.Text.Length < 3 || txbFullName.Text.Length > 100 || float.Parse(txbDTB.Text) > 10)
                {
                    if (IsStudentID(txbID.Text) == false)
                    {
                        //MessageBox.Show("Mã sinh viên gồm 10 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        MessageBox.Show("Mã sinh viên gồm 10 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    if (txbFullName.Text.Length < 3 || txbFullName.Text.Length > 100)
                    {
                        //MessageBox.Show("Tên sinh viên gồm 3 - 100 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        MessageBox.Show("Sai ho ten", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    if (float.Parse(txbDTB.Text) > 10)
                    {
                        //MessageBox.Show("Điểm trung bình từ 0 - 10", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        MessageBox.Show("Sai diem trung binh", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        Student s = new Student()
                        {
                            StudentID = txbID.Text,
                            FullName = txbFullName.Text,
                            AverageScore = double.Parse(txbDTB.Text),
                            FacultyID = int.Parse(cmbNganh.SelectedValue.ToString())
                        };
                        db.Students.Add(s);
                        db.SaveChanges();

                        BindGrid(db.Students.ToList());
                        clear();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            catch (Exception ex) 
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }
        private void clear()
        {
            txbID.Text = "";
            txbFullName.Text = "";
            cmbNganh.Text = "";
            txbDTB.Text = "";

        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            Student dbDelete = db.Students.FirstOrDefault(p => p.StudentID == txbID.Text);
            if (dbDelete != null)
            {
                db.Students.Remove(dbDelete);
                db.SaveChanges();// lưu thay dổi
                BindGrid(db.Students.ToList());
                MessageBox.Show("Xoá sinh viên thành công");

            }
            else
            {
                MessageBox.Show("không tìm thấy sinh viên");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Student dbUpdate = db.Students.FirstOrDefault(p => p.StudentID == txbID.Text);
            if (dbUpdate != null)
            {
                dbUpdate.FullName = txbFullName.Text;
                dbUpdate.AverageScore = double.Parse(txbDTB.Text);
                dbUpdate.FacultyID = (cmbNganh.SelectedItem as Faculty).FacultyID;

                db.SaveChanges(); //lưu thay đổi
                BindGrid(db.Students.ToList());
                MessageBox.Show("Sửa thông tin thành công!", "Thông báo!", MessageBoxButtons.OK);
                clear();
            }
            else
            {
                MessageBox.Show("Khong sua duoc!", " thong bao", MessageBoxButtons.OK);
            }
        }

        private void dtgvStudent_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dtgvStudent.Rows[e.RowIndex];
                txbID.Text = row.Cells[1].Value.ToString();
                txbFullName.Text = row.Cells[2].Value.ToString();
                txbDTB.Text = row.Cells[3].Value.ToString();
                cmbNganh.Text = row.Cells[4].Value.ToString();
            } 
        }

        private void quảnLýKhoaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuanLyKhoa ql = new QuanLyKhoa();
            ql.ShowDialog();
        }

        private void tìmKiếmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TimKiem tk = new TimKiem();
            tk.ShowDialog();
        }

        private void thốngKêToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fmReport report = new fmReport();
            report.ShowDialog();
        }
    }
}
