﻿using Lab04_28092023.Model;
using Lab04_28092023.Report;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab04_28092023
{
    public partial class fmReport : Form
    {
        public fmReport()
        {
            InitializeComponent();
        }

        private void fmReport_Load(object sender, EventArgs e)
        {
            StudentContextDB db = new StudentContextDB();

            var studenReportDTO = db.Students.Select(p => new ReportStudentDTO
            {
                StudentID = p.StudentID,
                FullName = p.FullName,
                AverageScore = p.AverageScore,
                FacultyName = p.Faculty.FacultyName,
            }).ToList();

            this.rptStudent.LocalReport.ReportPath = "./Report/StudentReport.rdlc";
            var reportDataSource = new ReportDataSource("StudentReportDataset", studenReportDTO);

            this.rptStudent.LocalReport.DataSources.Clear();
            this.rptStudent.LocalReport.DataSources.Add(reportDataSource);

            this.rptStudent.RefreshReport();

            this.rptStudent.RefreshReport();
           
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
