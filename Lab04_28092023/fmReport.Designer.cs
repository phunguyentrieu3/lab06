﻿namespace Lab04_28092023
{
    partial class fmReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptStudent = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rptStudent
            // 
            this.rptStudent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptStudent.LocalReport.ReportEmbeddedResource = "Lab04_28092023.Report.StudentReport.rdlc";
            this.rptStudent.Location = new System.Drawing.Point(0, 0);
            this.rptStudent.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rptStudent.Name = "rptStudent";
            this.rptStudent.ServerReport.BearerToken = null;
            this.rptStudent.Size = new System.Drawing.Size(655, 440);
            this.rptStudent.TabIndex = 0;
            this.rptStudent.Load += new System.EventHandler(this.reportViewer1_Load);
            // 
            // fmReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 440);
            this.Controls.Add(this.rptStudent);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "fmReport";
            this.Text = "fmReport";
            this.Load += new System.EventHandler(this.fmReport_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptStudent;
    }
}