﻿using Lab04_28092023.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab04_28092023
{
    public partial class QuanLyKhoa : Form
    {
        StudentContextDB db = new StudentContextDB();
        public QuanLyKhoa()
        {
            InitializeComponent();
            db = new StudentContextDB();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
           
            int x = int.Parse(txbMaKhoa.Text);
            Faculty dbDelete = db.Faculties.FirstOrDefault(p => p.FacultyID == x);
            if (dbDelete != null)
            {
                db.Faculties.Remove(dbDelete);
                db.SaveChanges();
                BindGrid(db.Faculties.ToList());
            }
        }
        private void BindGrid(List<Faculty> listFa)
        {
            dtgvKhoa.Rows.Clear();
            
            foreach (var item in listFa)
            {
                int index = dtgvKhoa.Rows.Add();
                dtgvKhoa.Rows[index].Cells[0].Value = item.FacultyID;
                dtgvKhoa.Rows[index].Cells[1].Value = item.FacultyName;
                dtgvKhoa.Rows[index].Cells[2].Value = item.TotalProfessor;  
            }
        }
        private void QuanLyKhoa_Load(object sender, EventArgs e)
        {
            try
            {
                List<Faculty> listFac = db.Faculties.ToList();
                BindGrid(listFac.ToList());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void btnInsertUpdate_Click(object sender, EventArgs e)
        {
            int i = int.Parse(txbMaKhoa.Text);
            bool a = db.Faculties.Any(s => s.FacultyID == i);
            try
            {
                if (a)
                {
                    Faculty update = db.Faculties.FirstOrDefault(p => p.FacultyID == i);
                    if (update != null)
                    {
                        update.FacultyName = txbMaKhoa.Text;
                        if (txbTongGS.Text != "")
                            update.TotalProfessor = int.Parse(txbTongGS.Text);
                    }
                    MessageBox.Show("Sửa thành công!");
                }
                else
                {
                    if (txbTongGS.Text != "")
                    {
                        Faculty f = new Faculty()
                        {
                            FacultyID = int.Parse(txbMaKhoa.Text),
                            FacultyName = txbTenKhoa.Text,
                            TotalProfessor = int.Parse(txbTongGS.Text),
                        };
                        db.Faculties.Add(f);
                        MessageBox.Show("Thêm thành công!");
                    }
                    else
                    {
                        Faculty f = new Faculty()
                        {
                            FacultyID = int.Parse(txbMaKhoa.Text),
                            FacultyName = txbTenKhoa.Text,

                        };
                        db.Faculties.Add(f);
                        MessageBox.Show("Thêm thành công!");
                    }
                }
                db.SaveChanges();

                BindGrid(db.Faculties.ToList());

                txbMaKhoa.Text = "";
                txbTenKhoa.Text = "";
                txbTongGS.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txbMaKhoa_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txbTongGS_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void dtgvKhoa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dtgvKhoa.Rows[e.RowIndex];
                txbMaKhoa.Text = row.Cells[0].Value.ToString();
                txbTenKhoa.Text = row.Cells[1].Value.ToString();
                txbTongGS.Text = row.Cells[2].Value.ToString();
                
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ban co muon thoat?", "Thong bao!", MessageBoxButtons.OK);
            Application.Exit();
        }
    }
}
